function inversa = invert(A)
	n = length(A);

	% Cand se ajunge la un element, 1/det(A) va fi 1 / "singurul element din A"
%	if n == 1
%		inversa = 1 / det(A);
%		return;
%	end

	% Cand se ajunge la 2 elemente, daca matricea e de forma [A11 A12 ; A21 A22],
	% atunci inversa ei este 1 / det(A) * [A22 -A12; -A21 A11]
%	if n == 2
%		inversa = 1 / (multiStrass(A(1, 1), A(2, 2)) - multiStrass(A(1, 2), A(2, 1))) * [A(2,2) -A(1,2); -A(2,1) A(1,1)];
%		return;
%	end
	if n <= 5
		inversa = gaussJord(A);
		return;
	end

	% Dupa formulele din laborator, apelam recursiv functia invert facand
	% toate inmultirile cu strassen
	if n > 5
		[A1 A2 A3 A4] = partition(A);

		A4inv = invert(A4);
		A1inv = invert(A1);

		X1 = invert(A1 - multiStrass(multiStrass(A3, A4inv), A2));
		X2 = multiStrass(multiStrass(-A4inv, A2), X1);
		X4 = invert(A4 - multiStrass(multiStrass(A2, A1inv), A3));
		X3 = multiStrass(multiStrass(-A1inv, A3), X4);

		inversa = [X1 X3; X2 X4];
	end
end
