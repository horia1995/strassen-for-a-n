function [a b m q] = trybord(A, B)
	[m n] = size(A);
	[p q] = size(B);

	maxLCSize = max(max(m, n), max(p, q));
	newSize = 2 ^ nextpow2(maxLCSize);

	% Daca ambele sunt patratice, de aceeasi dimensiune
	% si puteri ale lui 2, le returnez direct
	if m == newSize && n == newSize && p == newSize && q == newSize
		a = A;
		b = B;
		return;
	end

	% Altfel, le bordez
	a = zeros(newSize);
	b = zeros(newSize);

	a(1 : m, 1 : n) = A;
	b(1 : p, 1 : q) = B;
end
