function [A1 A2 A3 A4] = partition(A)
	n = length(A) ;
	middle = floor(n / 2);

	% Vreau sa partitionez plecand din mijlocul matricei
	% catre sus-stanga si jos-dreapta pe diagonale
	for count = 0 : middle
		i = middle - count;
		j = middle + count;

	% Verific apoi simultan valoarea determinantilor
	% pentru fiecare pozitie in care am ajuns
		if det(A(1 : i, 1 : i)) != 0 
			if det(A(i + 1 : n, i + 1 : n)) != 0
				A1 = A(1 : i, 1 : i);
				A2 = A(i + 1 : n, 1 : i);
				A3 = A(1 : i, i + 1 : n);
				A4 = A(i + 1 : n, i + 1 : n);
				break;
			end
		end

		if det(A(j + 1 : n, j + 1 : n)) != 0
			if det(A(1 : j, 1 : j)) != 0
				A1 = A(1 : j, 1 : j);
				A2 = A(j + 1 : n, 1 : j);
				A3 = A(1 : j, j + 1 : n);
				A4 = A(j + 1 : n, j + 1 : n);
				break;
			end
		end
	end
end
