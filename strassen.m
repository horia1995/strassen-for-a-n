function Result = strassen(fisier_in, fisier_out)
	fin = fopen(fisier_in, 'r');
	n = fscanf(fin, '%d', 1);
	A = [];
	k = 1;
	while ~feof(fin)
		A(k, :) = fscanf(fin, '%f') ;
		k++;
	end

	newSize = sqrt(length(A));
	A = reshape(A, [newSize, newSize]);
	len = length(A);

	% fscanf sub aceasta forma citeste trasnpusa, deci e necesar invert(A')
	inv = invert(A');
	Result = inv;
	for i = 1 : n - 1
		Result = multiStrass(Result, inv);
	end

	fout = fopen(fisier_out, 'w');
	for i = 1 : len
			fprintf(fout, '%f ', Result(i,:));
			fprintf(fout, "\n");
	end
	fclose(fout);
end
