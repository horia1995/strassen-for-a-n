function Result = multiStrass(A, B)
	[A B m q] = trybord(A, B);
	Result = _strassen(A, B)(1 : m, 1 : q);
end
