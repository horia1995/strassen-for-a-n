function Result = _strassen(A, B)

	% Algoritmul propriu-zis al lui strassen
	newSize = length(A);

	if newSize <= 64
		Result = A * B;

	else
		middle = newSize / 2;
		i = 1 : middle;
		j = middle + 1 : newSize;

		aii = A(i, i);
		ajj = A(j, j);
		aij = A(i, j);
		aji = A(j, i);

		bii = B(i, i);
		bjj = B(j, j);
		bij = B(i, j);
		bji = B(j, i);

		sum1 = _strassen(aii + ajj,	bii + bjj);
		sum2 = _strassen(aji + ajj,	bii);
		sum3 = _strassen(aii,		bij - bjj);
		sum4 = _strassen(ajj,		bji - bii);
		sum5 = _strassen(aii + aij,	bjj);
		sum6 = _strassen(aji - aii,	bii + bij);
		sum7 = _strassen(aij - ajj,	bji + bjj);

		Result = [ sum1 + sum4 - sum5 + sum7  sum3 + sum5;  sum2 + sum4  sum1 + sum3 - sum2 + sum6 ];
	end
end
