function invert = gaussJord(Matrix)
	[lin col] = size(Matrix);

	% Calculez inversa modulo 29 a matricei
	% Functia foloseste "inversa", functie
	% ce returneaza valoarea modulo 29 a
	% unui numar
	invert = eye(lin);
	for j = 1 : lin
		for i = j : lin
			if Matrix(i,j) != 0
				t = 1 / Matrix(j, j);
				for k = 1 : lin
					Matrix(j, k) = t * Matrix(j, k);
					invert(j, k) = t * invert(j, k);
				end
				for l = 1 : lin
					if l != j
						t = -Matrix(l, j);
						for k = 1 : lin
							Matrix(l, k) = Matrix(l, k) + t * Matrix(j, k);
							invert(l, k) = invert(l, k) + t * invert(j, k);
						end
					end
				end
			end
		end
	end
end
