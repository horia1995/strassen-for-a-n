My Octave/Matlab implementation of the Strassen algorithm, using a recursive partitioning function to invert the Matrix, which is supposed to calculate A^(-n)

This was homework during the Numerical Methods course I attended, and I only
managed to find the "strassen3.in" test file.

Octave
strassen ("strassen3.in", "strassen3.out")

Compare strassen3.out with the Octave implementation of A^(-n)